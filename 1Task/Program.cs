﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class Point
    {
        int x;
        int y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;

        }
        public static Point operator +(Point a, Point b)
        {
            a.x += b.x;
            a.y += b.y;
            return a;
        }

        public override string ToString()
        {
           return x.ToString() + " " + y.ToString();
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point (10, 12);
            Point b = new Point (1, 2);
            Point c = a + b;
            Console.WriteLine(c);
            Console.ReadKey();

        }
    }

}
